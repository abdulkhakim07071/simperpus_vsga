<?php
include 'includes/header.php';
include 'koneksi.php';

// Fungsi untuk mendapatkan data transaksi dengan status pinjam
function getDataTransaksiPinjam($search_nama) {
    global $conn;
    $status = "pinjam";

    // Buat query pencarian berdasarkan nama anggota dengan menggunakan LIKE
    $query = "SELECT t.*, a.nama AS nama_anggota, b.judul_buku FROM transaksi t 
              JOIN anggota a ON t.id_anggota = a.id 
              JOIN buku b ON t.buku = b.id_buku 
              WHERE t.status = '$status' AND a.nama LIKE '%$search_nama%'";

    $result = $conn->query($query);

    // Periksa apakah query berjalan dengan benar
    if (!$result) {
        die("Error: " . $conn->error);
    }

    return $result;
}

// Fungsi untuk menyimpan data transaksi pengembalian buku
function simpanPengembalian($transaksi_id) {
    global $conn;
    $status = "kembali";

    $query = "UPDATE transaksi SET status='$status' WHERE id_transaksi='$transaksi_id'";
    if ($conn->query($query) === TRUE) {
        return true;
    } else {
        return false;
    }
}

// Cek apakah ada data yang dikirim melalui form pengembalian buku
if (isset($_POST['submit_pengembalian'])) {
    $transaksi_id = $_POST['transaksi_id'];

    if (simpanPengembalian($transaksi_id)) {
        echo "Pengembalian buku berhasil.";
    } else {
        echo "Pengembalian buku gagal.";
    }
}

// Mendapatkan nilai dari input pencarian
$search_nama = isset($_GET['search_nama']) ? $_GET['search_nama'] : "";

// Mendapatkan data transaksi dengan status pinjam dan filter berdasarkan nama anggota
$transaksiPinjamData = getDataTransaksiPinjam($search_nama);

?>

<!-- Form input pengembalian buku -->
<h3>Input Pengembalian Buku</h3>
<form method="post" class="col-4" action="">
    <div class="form-floating mb-3 col justify-content-center d-flex">
        <input type="text" class="form-control" class="justify-self-center d-flex" style="margin-right:20px;" id="transaksi_id" name="transaksi_id" placeholder="Masukkan ID Transaksi">
        <label for="transaksi_id">ID Transaksi</label>
        <input type="submit" class="btn btn-outline-success" style="height: 55px;" name="submit_pengembalian" value="Kembalikan Buku">
    </div>
</form>

<h3>Data Transaksi (Status Pinjam)</h3>

<!-- Form pencarian berdasarkan nama anggota -->
<form class="col-4 d-flex" method="get">
    <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search" id="search_nama" name="search_nama" value="<?php echo $search_nama; ?>">
    <button class="btn btn-outline-success" type="submit" for="search_nama">Cari</button>
</form>

<br><br><br>

<!-- Tabel data transaksi dengan status pinjam -->
<table class="table">
    <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">ID Transaksi</th>
            <th scope="col">Nama Anggota</th>
            <th scope="col">Judul Buku</th>
            <th scope="col">Tanggal Pinjam</th>
        </tr>
    </thead>
    <tbody>
        <?php
        // Jumlah data per halaman
        $data_per_halaman = 5;

        // Hitung total halaman berdasarkan jumlah data
        $total_halaman = ceil($transaksiPinjamData->num_rows / $data_per_halaman);

        // Cek apakah ada halaman yang aktif
        $halaman_aktif = isset($_GET['halaman']) ? $_GET['halaman'] : 1;

        // Tentukan batas awal data yang ditampilkan pada halaman yang aktif
        $batas_awal = ($halaman_aktif - 1) * $data_per_halaman;
        $status = "pinjam";

        // Ambil data untuk halaman yang aktif
        $query_paginasi = "SELECT t.*, a.nama AS nama_anggota, b.judul_buku FROM transaksi t 
                            JOIN anggota a ON t.id_anggota = a.id 
                            JOIN buku b ON t.buku = b.id_buku 
                            WHERE t.status = '$status' AND a.nama LIKE '%$search_nama%'
                            LIMIT $batas_awal, $data_per_halaman";
        $result_paginasi = $conn->query($query_paginasi);

        // Tampilkan data transaksi pada halaman yang aktif
        $number = ($halaman_aktif - 1) * $data_per_halaman + 1;
        while ($row = $result_paginasi->fetch_assoc()) {
            echo "<tr>";
            echo "<td>" . $number . "</td>";
            echo "<td>" . $row['id_transaksi'] . "</td>";
            echo "<td>" . $row['nama_anggota'] . "</td>";
            echo "<td>" . $row['judul_buku'] . "</td>";
            echo "<td>" . $row['tanggal_pinjam'] . "</td>";
            echo "</tr>";
            $number++;
        }
        ?>
    </tbody>
</table>

<!-- Tampilkan pagination -->
<div class="col-12">
    <ul class="pagination justify-content-center">
        <?php
        // Tampilkan tombol halaman sebelumnya
        if ($halaman_aktif > 1) {
            echo '<li class="page-item"><a class="page-link" href="?halaman=' . ($halaman_aktif - 1) . '&search_nama=' . $search_nama . '">Previous</a></li>';
        }

        // Tampilkan tombol halaman selanjutnya
        if ($halaman_aktif < $total_halaman) {
            echo '<li class="page-item"><a class="page-link" href="?halaman=' . ($halaman_aktif + 1) . '&search_nama=' . $search_nama . '">Next</a></li>';
        }
        ?>
    </ul>
</div>

<?php include 'includes/footer.php'; ?>
