<?php
include 'includes/header.php';
include 'koneksi.php';

// Fungsi untuk mendapatkan data anggota berdasarkan kata kunci pencarian
function searchAnggota($keyword) {
    global $conn;
    $query = "SELECT * FROM anggota WHERE nama LIKE '%$keyword%'";
    $result = $conn->query($query);
    return $result;
}

// Fungsi untuk menghapus data anggota berdasarkan ID
function deleteAnggota($id_anggota) {
    global $conn;
    $query = "DELETE FROM anggota WHERE id_anggota = '$id_anggota'";
    return $conn->query($query);
}

// Cek apakah ada data yang dikirim melalui form delete anggota
if (isset($_POST['delete_anggota'])) {
    $id_anggota = $_POST['id_anggota'];

    if (deleteAnggota($id_anggota)) {
        echo "Data anggota berhasil dihapus.";
    } else {
        echo "Gagal menghapus data anggota.";
    }
}

// Mendapatkan nilai dari input pencarian
$search_keyword = isset($_GET['search_keyword']) ? $_GET['search_keyword'] : "";

// Mendapatkan data anggota berdasarkan kata kunci pencarian
$anggotaData = searchAnggota($search_keyword);

// Jumlah data per halaman
$data_per_halaman = 5;

// Hitung total halaman berdasarkan jumlah data
$total_halaman = ceil($anggotaData->num_rows / $data_per_halaman);

// Cek apakah ada halaman yang aktif
$halaman_aktif = isset($_GET['halaman']) ? $_GET['halaman'] : 1;

// Tentukan batas awal data yang ditampilkan pada halaman yang aktif
$batas_awal = ($halaman_aktif - 1) * $data_per_halaman;

// Ambil data untuk halaman yang aktif
$query_paginasi = "SELECT * FROM anggota WHERE nama LIKE '%$search_keyword%' LIMIT $batas_awal, $data_per_halaman";
$result_paginasi = $conn->query($query_paginasi);

?>

<div class="container-fluid">
    <h2>Tampil Anggota</h2>

    <!-- Form pencarian berdasarkan nama anggota -->
    <nav class="navbar bg-body-tertiary">
        <div class="container-fluid">
            <form class="d-flex" method="get">
                <input class="form-control me-2" name="search_keyword" id="search_keyword" type="text" value="<?php echo $search_keyword; ?>" style=" margin-right: 20px;" placeholder="Cari Nama Anggota" aria-label="Search">
                <button class="btn btn-outline-success" type="submit">Search</button>
            </form>
        </div>
    </nav>

    <div class="row">
        <!-- Tabel data anggota -->
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">ID_Anggota</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Jenis Kelamin</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">foto</th>
                    <th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>

                <?php
                $number = ($halaman_aktif - 1) * $data_per_halaman + 1;
                if ($result_paginasi->num_rows > 0) {
                    while ($row = $result_paginasi->fetch_assoc()) {
                        echo "<tr>";
                        echo "<th scope='row'>" . $number . "</th>";
                        echo "<td>" . $row['id_anggota'] . "</td>";
                        echo "<td>" . $row['nama'] . "</td>";
                        echo "<td>" . $row['jeniskelamin'] . "</td>";
                        echo "<td>" . $row['alamat'] . "</td>";
                        echo "<td><img src='app/images/" . $row['foto'] . "' style='width: 100px; height: 100px;' alt='Foto Anggota'></td>";
                        echo "<td>
                            <a href='edit_anggota.php?id=" . $row['id_anggota'] . "'><button type='submit' name='edit_anggota' class='btn btn-warning'>edit</button></a>

                            <form method='post' style='display: inline-block;'>
                            <input type='hidden' name='id_anggota' value='" . $row['id_anggota'] . "'>
                            <button type='submit' name='delete_anggota' class='btn btn-danger'>delete</button></a>
                            </form>
                            </td>";
                        echo "</tr>";
                        $number++;
                    }
                } else {
                    echo "<tr><td colspan='6'>Tidak ada data anggota atau tidak ditemukan berdasarkan pencarian.</td></tr>";
                }
                ?>
            </tbody>
        </table>

        <br><br><br><br><br><br>
    </div>

    <div class="row justify-content-center">
        <ul class="pagination">
            <?php
            // Tampilkan tombol halaman sebelumnya
            if ($halaman_aktif > 1) {
                echo '<li class="page-item"><a class="page-link" href="?halaman=' . ($halaman_aktif - 1) . '&search_keyword=' . $search_keyword . '">Previous</a></li>';
            }

            // Tampilkan tombol halaman
            for ($i = 1; $i <= $total_halaman; $i++) {
                if ($i == $halaman_aktif) {
                    echo '<li class="page-item active"><a class="page-link" href="?halaman=' . $i . '&search_keyword=' . $search_keyword . '">' . $i . '</a></li>';
                } else {
                    echo '<li class="page-item"><a class="page-link" href="?halaman=' . $i . '&search_keyword=' . $search_keyword . '">' . $i . '</a></li>';
                }
            }

            // Tampilkan tombol halaman selanjutnya
            if ($halaman_aktif < $total_halaman) {
                echo '<li class="page-item"><a class="page-link" href="?halaman=' . ($halaman_aktif + 1) . '&search_keyword=' . $search_keyword . '">Next</a></li>';
            }
            ?>
        </ul>
    </div>
    
    <div class="row justify-content-center">
        <a href="data_input.php"><button type="button" class="btn btn-primary">Tambah Anggota</button></a>
    </div>

</div>

<?php include 'includes/footer.php'; ?>
