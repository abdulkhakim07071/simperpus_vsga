<?php
session_start();

// Cek apakah pengguna sudah login
if (!isset($_SESSION['id_pengguna'])) {
    // Redirect ke halaman login jika belum login
    header('Location: login.php');
    exit();
}

// Fungsi untuk logout
function logout() {
    session_unset(); // Unset all session variables
    session_destroy(); // Destroy the session
    header('Location: login.php');
    exit();
}

// Cek apakah data logout sudah dikirim melalui URL
if (isset($_GET['logout'])) {
    logout();
}
?>
<!-- header.php -->
<!-- header.php -->
<!DOCTYPE html>
<html>
<head>
    <title>Sistem Informasi Perpustakaan</title>
    <link rel="stylesheet" type="text/css" href="src/css/style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="src/js/script.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
  
    <script>
  // Fungsi untuk menambahkan kelas 'active' ke link yang aktif
  function setActiveLink() {
    var links = document.querySelectorAll('.nav-link');
    links.forEach(function(link) {
      if (link.href === window.location.href) {
        link.classList.add('active');
      } else {
        link.classList.remove('active');
      }
    });
  }

  // Panggil fungsi saat halaman selesai dimuat
  document.addEventListener('DOMContentLoaded', function() {
    setActiveLink();
  });
</script>
<script>
    // Fungsi untuk menambahkan kelas 'active' ke elemen dropdown dan induknya
    function setActiveDropdown() {
        var dropdowns = document.querySelectorAll('.dropdown');
        dropdowns.forEach(function(dropdown) {
            var dropdownToggle = dropdown.querySelector('.dropdown-toggle');
            var dropdownMenu = dropdown.querySelector('.dropdown-menu');

            // Tambahkan kelas 'active' ke elemen dropdown jika salah satu opsi dipilih
            dropdownMenu.addEventListener('click', function(event) {
                var target = event.target;
                if (target.classList.contains('dropdown-item')) {
                    dropdownToggle.classList.add('active');
                }
            });
        });
    }

    // Panggil fungsi saat halaman selesai dimuat
    document.addEventListener('DOMContentLoaded', function() {
        setActiveDropdown();
    });
</script>

  </head>
<body>

<header>
<nav class="navbar navbar-expand-lg bg-body-tertiary">
<div class="container-fluid">

<ul class="nav nav-pills">
  <li class="nav-item">
    <a class="nav-link <?php echo ($_SERVER['PHP_SELF'] == '/index.php' ? 'active' : ''); ?>" href="index.php">Dashboard</a>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle <?php echo (strpos($_SERVER['PHP_SELF'], '/data_master/') !== false ? 'active' : ''); ?>" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
          Data Master
        </a>
          <ul class="dropdown-menu">
              <li><a class="dropdown-item" href="tampil_anggota.php">Data Angggota</a></li>
            <li><a class="dropdown-item" href="tampil_Buku.php">Data Buku</a></li>
        </ul>
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle <?php echo (strpos($_SERVER['PHP_SELF'], '/data_transaksi/') !== false ? 'active' : ''); ?>" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Data Transaksi
        </a>
        <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="data_transaksi.php">Transaksi Peminjaman</a></li>
            <li><a class="dropdown-item" href="kembali_buku.php">Transaksi Pengembalian</a></li>
        </ul>
    </li>
    <li class="nav-item">
      <a class="nav-link <?php echo ($_SERVER['PHP_SELF'] == '/lap_transaksi.php' ? 'active' : ''); ?>" href="lap_transaksi.php">Laporan Transaksi</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="?logout">Logout</a>
    </li>
</ul>
</div>
</nav>

</header>


