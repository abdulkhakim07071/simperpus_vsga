<?php
include 'includes/header.php';
include 'koneksi.php';

// Fungsi untuk mendapatkan data anggota berdasarkan ID
function getAnggotaByID($id_anggota) {
    global $conn;
    $query = "SELECT * FROM anggota WHERE id_anggota = '$id_anggota'";
    $result = $conn->query($query);
    return $result->fetch_assoc();
}

// Cek apakah ada data yang dikirim melalui form edit anggota
if (isset($_POST['edit_anggota'])) {
    $id_anggota = $_POST['id_anggota'];
    $nama = $_POST['nama'];
    $jeniskelamin = $_POST['jeniskelamin'];
    $alamat = $_POST['alamat'];

    // Update data anggota berdasarkan ID
    $query = "UPDATE anggota SET nama = '$nama', jeniskelamin = '$jeniskelamin', alamat = '$alamat' WHERE id_anggota = '$id_anggota'";
    if ($conn->query($query) === TRUE) {
        echo "Data anggota berhasil diupdate.";
    } else {
        echo "Gagal mengupdate data anggota: " . $conn->error;
    }
}

// Mendapatkan ID anggota dari parameter URL
$id_anggota = $_GET['id'];
$anggotaData = getAnggotaByID($id_anggota);
?>

<h2>Edit Anggota</h2>

<form method="post">
    <input type="hidden" name="id_anggota" value="<?php echo $id_anggota; ?>">
    <label for="nama">Nama:</label>
    <input type="text" name="nama" id="nama" value="<?php echo $anggotaData['nama']; ?>" required>
    <label for="jeniskelamin">Jenis Kelamin:</label>
    <input type="text" name="jeniskelamin" id="jeniskelamin" value="<?php echo $anggotaData['jeniskelamin']; ?>" required>
    <label for="alamat">Alamat:</label>
    <input type="text" name="alamat" id="alamat" value="<?php echo $anggotaData['alamat']; ?>" required>
    <button type="submit" name="edit_anggota">Simpan Perubahan</button>
</form>

<?php include 'includes/footer.php'; ?>
