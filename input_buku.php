<!-- data_input.php -->
<?php
include 'includes/header.php';
include 'koneksi.php';

// Function untuk menyimpan data buku
function simpanBuku($id_buku, $judul_buku, $ISBN, $pengarang, $penerbit, $tahun) {
    global $conn;

    $query = "INSERT INTO buku (id_buku, judul_buku, ISBN, pengarang, penerbit, tahun) VALUES ('$id_buku', '$judul_buku', '$ISBN', '$pengarang', '$penerbit', '$tahun')";
    if ($conn->query($query) === TRUE) {
        return true;
    } else {
        return false;
    }
}

// Cek apakah ada data yang dikirim melalui form input buku
if (isset($_POST['submit_buku'])) {
    $id_buku = $_POST['id_buku'];
    $judul_buku = $_POST['judul_buku'];
    $ISBN = $_POST['ISBN'];
    $pengarang = $_POST['pengarang'];
    $penerbit = $_POST['penerbit'];
    $tahun = $_POST['tahun'];

    if (simpanBuku($id_buku, $judul_buku, $ISBN, $pengarang, $penerbit, $tahun)) {
        echo "Data buku berhasil disimpan.";
    } else {
        echo "Data buku gagal disimpan.";
    }
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col text-center"> <!-- Tambahkan class text-center di sini -->
            <h2>Input Data Buku</h2>
            <br><br>
        </div>
    </div>

<!-- Form input buku -->
<div class="row input">


    <form method="post" action=""  enctype="multipart/form-data">
        <!-- Tambahkan atribut "action" dengan nilai "data_input.php" agar form akan mengirimkan data ke halaman ini -->
            <div class="form-floating mb-3 col-4 justify-content-center d-flex">
                <input type="text" class="form-control" class="justify-self-center d-flex" id="id_buku" name="id_buku" placeholder="Masukkan ID Buku">
                <label for="id_buku">ID Buku</label>
            </div>
            <div class="form-floating mb-3 col-4 justify-content-center d-flex">
                <input type="text" class="form-control" class="justify-self-center d-flex" id="judul_buku" name="judul_buku" placeholder="Masukkan Judul Buku">
                <label for="judul_buku">Judul Buku</label>
            </div>
            <div class="form-floating mb-3 col-4 justify-content-center d-flex">
                <input type="text" class="form-control" class="justify-self-center d-flex" id="ISBN" name="ISBN" placeholder="Masukkan ISBN">
                <label for="ISBN">ISBN</label>
            </div>
            <div class="form-floating mb-3 col-4 justify-content-center d-flex">
                <input type="text" class="form-control" class="justify-self-center d-flex" id="pengarang" name="pengarang" placeholder="Masukkan Nama Pengarang">
                <label for="pengarang">Pengarang</label>
            </div>
            <div class="form-floating mb-3 col-4 justify-content-center d-flex">
                <input type="text" class="form-control" class="justify-self-center d-flex" id="penerbit" name="penerbit" placeholder="Masukkan Nama penerbit">
                <label for="penerbit">Penerbit</label>
            </div>
            <div class="form-floating mb-3 col-4 justify-content-center d-flex">
                <input type="text" class="form-control" class="justify-self-center d-flex" id="tahun" name="tahun" placeholder="Masukkan Tahun">
                <label for="tahun">Tahun Terbit</label>
            </div>
            <div class="col-4">
                
                <button class="btn btn-outline-success " type="submit" name="submit_buku" value="Simpan Buku">Submit</button>
            </div>
            
        </form>
    </div>
</div>
    
    <?php include 'includes/footer.php'; ?>
    