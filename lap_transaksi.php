<?php
include 'includes/header.php';
include 'koneksi.php';
require 'vendor/autoload.php'; // Menggunakan autoload dari Composer untuk memuat PhpSpreadsheet

// Fungsi untuk mendapatkan data transaksi dengan status kembali
function getDataTransaksiKembali() {
    global $conn;
    $status = "kembali";
    $query = "SELECT t.*, a.nama AS nama_anggota, b.judul_buku FROM transaksi t JOIN anggota a ON t.id_anggota = a.id JOIN buku b ON t.buku = b.id_buku WHERE t.status = '$status'";
    $result = $conn->query($query);
    return $result->fetch_all(MYSQLI_ASSOC); // Menggunakan fetch_all untuk mendapatkan seluruh data sebagai array asosiatif
}

// Cek apakah tombol cetak laporan diklik
if (isset($_POST['cetak_laporan'])) {
    cetakLaporan();
}

// Function untuk mencetak laporan ke format excel
function cetakLaporan() {
    global $conn;

    // Ambil data transaksi dengan status kembali
    $status = "kembali";
    $query = "SELECT t.*, a.nama AS nama_anggota, b.judul_buku FROM transaksi t JOIN anggota a ON t.id_anggota = a.id JOIN buku b ON t.buku = b.id_buku WHERE t.status = '$status'";
    $result = $conn->query($query);

    if ($result->num_rows > 0) {
        // Menggunakan PhpSpreadsheet
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        // Header laporan
        $sheet->setCellValue('A1', 'ID Transaksi');
        $sheet->setCellValue('B1', 'Nama Anggota');
        $sheet->setCellValue('C1', 'Judul Buku');
        $sheet->setCellValue('D1', 'Tanggal Pinjam');

        // Data laporan
        $row = 2;
        while ($row_data = $result->fetch_assoc()) {
            $sheet->setCellValue('A' . $row, $row_data['id_transaksi']);
            $sheet->setCellValue('B' . $row, $row_data['nama_anggota']);
            $sheet->setCellValue('C' . $row, $row_data['judul_buku']);
            $sheet->setCellValue('D' . $row, $row_data['tanggal_pinjam']);
            $row++;
        }

        // Mengatur judul dan format file
        $fileName = 'laporan_transaksi_' . date('Y-m-d') . '.xlsx';

        // Simpan laporan ke dalam file
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $writer->save($fileName);

        // Set header untuk download file
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'"');
        header('Cache-Control: max-age=0');
        ob_end_clean();

        // Mengirimkan file ke output
        $writer->save('php://output');
        exit; // Hentikan eksekusi kode setelah cetak laporan selesai
    } else {
        echo "Tidak ada data transaksi dengan status kembali.";
    }
}
?>

<h2>Laporan Transaksi dengan Status Kembali</h2>

<!-- Tabel data transaksi dengan status kembali -->
<table>
    <thead>
        <tr>
            <th>ID Transaksi</th>
            <th>Nama Anggota</th>
            <th>Judul Buku</th>
            <th>Tanggal Pinjam</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $transaksiKembaliData = getDataTransaksiKembali();
        if ($transaksiKembaliData) {
            foreach ($transaksiKembaliData as $row) {
                echo "<tr>";
                echo "<td>" . $row['id_transaksi'] . "</td>";
                echo "<td>" . $row['nama_anggota'] . "</td>";
                echo "<td>" . $row['judul_buku'] . "</td>";
                echo "<td>" . $row['tanggal_pinjam'] . "</td>";
                echo "</tr>";
            }
        } else {
            echo "<tr><td colspan='4'>Tidak ada data transaksi dengan status kembali.</td></tr>";
        }
        ?>
    </tbody>
</table>

<!-- Tombol untuk mencetak laporan ke format excel -->
<form method="post">
    <button type="submit" name="cetak_laporan">Cetak Laporan ke Excel</button>
</form>

<?php include 'includes/footer.php'; ?>
