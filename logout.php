<?php
session_start();

// Panggil fungsi logout
logout();

// Redirect ke halaman login setelah logout
header("Location: login.php");
exit;
?>
