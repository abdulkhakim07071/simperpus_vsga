<?php
include 'includes/header.php';
include 'koneksi.php';

// Fungsi untuk mengambil data anggota dan buku
function getAnggotaData() {
    global $conn;
    $query = "SELECT * FROM anggota";
    $result = $conn->query($query);
    return $result;
}

function getBukuData() {
    global $conn;
    $query = "SELECT * FROM buku";
    $result = $conn->query($query);
    return $result;
}

// Fungsi untuk menyimpan data transaksi peminjaman buku
function simpanPeminjaman($anggota, $buku, $tanggal) {
    global $conn;
    $status = "pinjam";

    $query = "INSERT INTO transaksi (id_anggota, buku, tanggal_pinjam, status) VALUES ('$anggota', '$buku', '$tanggal', '$status')";
    if ($conn->query($query) === TRUE) {
        return true;
    } else {
        return false;
    }
}

// Fungsi untuk menyimpan data transaksi pengembalian buku

// Cek apakah ada data yang dikirim melalui form peminjaman buku
if (isset($_POST['submit_peminjaman'])) {
    $anggota = $_POST['anggota'];
    $buku = $_POST['buku'];
    $tanggal_pinjam = $_POST['tanggal_pinjam'];

    if (simpanPeminjaman($anggota, $buku, $tanggal_pinjam)) {
        echo "Peminjaman berhasil disimpan.";
    } else {
        echo "Peminjaman gagal disimpan.";
    }
}

// Cek apakah ada data yang dikirim melalui form pengembalian buku
if (isset($_POST['submit_pengembalian'])) {
    $transaksi_id = $_POST['transaksi_id'];

    if (simpanPengembalian($transaksi_id)) {
        echo "Pengembalian buku berhasil.";
    } else {
        echo "Pengembalian buku gagal.";
    }
}
?>

<div class="container-flex">
    <div class="row text-center">

        <h2>Data Transaksi</h2>
        <br><br><br>
    </div>
    <div class="row input">

 
        <form method="post" action="" enctype="multipart/form-data">
            <div class="mb-3 col-4 justify-content-center d-flex">
                <select name="anggota" id="anggota" type="text" class="form-select"required>
    <option selected>Anggota</option>
                    <?php
                    $anggotaData = getAnggotaData();
                    if ($anggotaData->num_rows > 0) {
                        while ($row = $anggotaData->fetch_assoc()) {
                            echo "<option value='" . $row['id'] . "'>" . $row['nama'] . "</option>";
                        }
                    }
                    ?>
                </select>
                
            </div>
            <div class=" mb-3 col-4 justify-content-center d-flex">
                <select name="buku" id="buku" type="text" class="form-select" required>
                <option selected>Buku</option>

                    <?php
        $bukuData = getBukuData();
        if ($bukuData->num_rows > 0) {
            while ($row = $bukuData->fetch_assoc()) {
                echo "<option value='" . $row['id_buku'] . "'>" . $row['judul_buku'] . "</option>";
            }
        }
        ?>
                    ?>
                </select>
            </div>
            
            <div class="form-floating mb-3 col-4 justify-content-center d-flex">
                <input type="date" class="form-control" class="justify-self-center d-flex" id="tanggal_pinjam" name="tanggal_pinjam" placeholder="Masukkan Anggota" required>
                <label for="tanggal_pinjam">Tanggal Peminjaman</label>
            </div>
            <div class="col-4 tombol">
            <button class="btn btn-outline-success " type="submit" name="submit_peminjaman" value="Pinjam Buku">Submit</button>

            </div>
        </div>
            
        </form>
        
        <!-- Form input pengembalian buku -->
    </div>
        <?php include 'includes/footer.php'; ?>
