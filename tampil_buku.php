<?php
include 'includes/header.php';
include 'koneksi.php';

// Fungsi untuk mendapatkan data buku berdasarkan kata kunci pencarian
function searchBuku($keyword) {
    global $conn;
    $query = "SELECT * FROM buku WHERE judul_buku LIKE '%$keyword%'";
    $result = $conn->query($query);
    return $result;
}

// Fungsi untuk menghapus data buku berdasarkan ID
function deleteBuku($id_buku) {
    global $conn;
    $query = "DELETE FROM buku WHERE id_buku = '$id_buku'";
    return $conn->query($query);
}

// Cek apakah ada data yang dikirim melalui form delete buku
if (isset($_POST['delete_buku'])) {
    $id_buku = $_POST['id_buku'];

    if (deleteBuku($id_buku)) {
        echo "Data buku berhasil dihapus.";
    } else {
        echo "Gagal menghapus data buku.";
    }
}

// Mendapatkan nilai dari input pencarian
$search_keyword = isset($_GET['search_keyword']) ? $_GET['search_keyword'] : "";

// Mendapatkan data buku berdasarkan kata kunci pencarian
$bukuData = searchBuku($search_keyword);

// Jumlah data per halaman
$data_per_halaman = 5;

// Hitung total halaman berdasarkan jumlah data
$total_halaman = ceil($bukuData->num_rows / $data_per_halaman);

// Cek apakah ada halaman yang aktif
$halaman_aktif = isset($_GET['halaman']) ? $_GET['halaman'] : 1;

// Tentukan batas awal data yang ditampilkan pada halaman yang aktif
$batas_awal = ($halaman_aktif - 1) * $data_per_halaman;

// Ambil data untuk halaman yang aktif
$query_paginasi = "SELECT * FROM buku WHERE judul_buku LIKE '%$search_keyword%' LIMIT $batas_awal, $data_per_halaman";
$result_paginasi = $conn->query($query_paginasi);
?>

<div class="container-fluid">
    <div class="row">
        <h2>Tampil Buku</h2>
        <br>

        <!-- Form pencarian berdasarkan judul buku -->
        <nav class="navbar bg-body-tertiary">
            <div class="container-fluid">
                <form class="d-flex" method="get">
                    <input class="form-control me-2" name="search_keyword" style=" margin-right: 20px;" id="search_keyword" type="text" value="<?php echo $search_keyword; ?>" placeholder="Cari Nama Buku" aria-label="Search">
                    <button class="btn btn-outline-success" type="submit">Search</button>
                </form>
            </div>
        </nav>
    </div>
    <div class="row">

        <!-- Tabel data buku -->
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">ID Buku</th>
                    <th scope="col">Judul Buku</th>
                    <th scope="col">ISBN</th>
                    <th scope="col">Pengarang</th>
                    <th scope="col">Penerbit</th>
                    <th scope="col">Tahun</th>
                    <th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $number = ($halaman_aktif - 1) * $data_per_halaman + 1;
                if ($result_paginasi->num_rows > 0) {
                    while ($row = $result_paginasi->fetch_assoc()) {
                        echo "<tr>";
                        echo "<td>" . $number . "</td>";
                        echo "<td>" . $row['id_buku'] . "</td>";
                        echo "<td>" . $row['judul_buku'] . "</td>";
                        echo "<td>" . $row['ISBN'] . "</td>";
                        echo "<td>" . $row['pengarang'] . "</td>";
                        echo "<td>" . $row['penerbit'] . "</td>";
                        echo "<td>" . $row['tahun'] . "</td>";
                        echo "<td>
                            <a href='edit_buku.php?id=" . $row['id_buku'] . "'><button type='submit' name='edit_anggota' class='btn btn-warning'>edit</button></a>
                            <form method='post' style='display: inline-block;'>
                                <input type='hidden' name='id_buku' value='" . $row['id_buku'] . "'>
                                <button type='submit' name='delete_buku' class='btn btn-danger'>delete</button>
                            </form>
                        </td>";
                        echo "</tr>";
                        $number++;
                    }
                } else {
                    echo "<tr><td colspan='8'>Tidak ada data buku atau tidak ditemukan berdasarkan pencarian.</td></tr>";
                }
                ?>
            </tbody>
        </table>
    </div>
    <br><br><br><br><br><br>

    <div class="row justify-content-center">
        <ul class="pagination">
            <?php
            // Tampilkan tombol halaman sebelumnya
            if ($halaman_aktif > 1) {
                echo '<li class="page-item"><a class="page-link" href="?halaman=' . ($halaman_aktif - 1) . '&search_keyword=' . $search_keyword . '">Previous</a></li>';
            }

            // Tampilkan tombol halaman
            for ($i = 1; $i <= $total_halaman; $i++) {
                if ($i == $halaman_aktif) {
                    echo '<li class="page-item active"><a class="page-link" href="?halaman=' . $i . '&search_keyword=' . $search_keyword . '">' . $i . '</a></li>';
                } else {
                    echo '<li class="page-item"><a class="page-link" href="?halaman=' . $i . '&search_keyword=' . $search_keyword . '">' . $i . '</a></li>';
                }
            }

            // Tampilkan tombol halaman selanjutnya
            if ($halaman_aktif < $total_halaman) {
                echo '<li class="page-item"><a class="page-link" href="?halaman=' . ($halaman_aktif + 1) . '&search_keyword=' . $search_keyword . '">Next</a></li>';
            }
            ?>
        </ul>
    </div>

    <div class="row justify-content-center">
        <a href="input_buku.php"><button type="button" class="btn btn-primary">Tambah Buku</button></a>
    </div>
</div>

<?php include 'includes/footer.php'; ?>
