<?php
session_start();
include 'koneksi.php';


// Fungsi untuk melakukan proses login
function login($username, $password) {
    global $conn;

    // Melakukan sanitasi input pengguna untuk mencegah SQL injection
    $username = mysqli_real_escape_string($conn, $username);
    $password = mysqli_real_escape_string($conn, $password);

    // Melakukan hash pada password yang dimasukkan oleh pengguna
    // $hashed_password = md5($password);

    // Cek apakah ada pengguna dengan username dan password yang sesuai dalam tabel pengguna
    $query = "SELECT id_pengguna FROM pengguna WHERE nama_pengguna = '$username' AND kata_sandi = '$password'";
    $result = $conn->query($query);

    if ($result->num_rows == 1) {
        // Login berhasil, simpan ID pengguna dalam sesi
        $row = $result->fetch_assoc();
        $_SESSION['id_pengguna'] = $row['id_pengguna'];
        return true;
    } else {
        // Login gagal
        return false;
    }
}

// Cek apakah data pengguna sudah dikirim melalui form login
if (isset($_POST['submit_login'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    if (login($username, $password)) {
        // Redirect ke halaman dashboard atau halaman lain yang sesuai
        header('Location: index.php');
        exit();
    } else {
        $login_error = "Username atau password salah.";
    }
}
?>



<!DOCTYPE html>
<html>

<head>
    <title>Halaman Login</title>
    <link rel="stylesheet" type="text/css" href="src/css/style.css">

</head>

<body>

    <div class="center">
        <h2>Login</h2>
        <?php if (isset($login_error)) { echo "<p style='color: red;'>$login_error</p>"; } ?>
    </div>
    <div class="center">

        <div class="border">

            <form id="loginForm" method="post" action="login.php">
                <div class="error" style="display: none"></div>
                <div class="input-field">
                    <label for="username" id="form">Email</label> <br>     
                    <input type="text" id="username" name="username" class="inputt" placeholder="Masukkan Username" required>
                </div>
                <br>
                <br>
                <div class="input-field">
                    <label for="password">Password</label><br>
                    <input type="password" id="password" name="password" class="inputt" placeholder="Masukkan Password" required>
                </div>
                <div class="kolom">
                    <button type="submit" class="tomboll" name="submit_login" value="login"> Login</button>
                </div>
            </form>
        </div>
    </div>

</body>

</html>
