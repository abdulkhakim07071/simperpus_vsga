<?php
include 'includes/header.php';
include 'koneksi.php';

// Fungsi untuk mendapatkan data buku berdasarkan ID
function getBukuByID($id_buku) {
    global $conn;
    $query = "SELECT * FROM buku WHERE id_buku = '$id_buku'";
    $result = $conn->query($query);
    return $result->fetch_assoc();
}

// Cek apakah ada data yang dikirim melalui form edit buku
if (isset($_POST['edit_buku'])) {
    $id_buku = $_POST['id_buku'];
    $judul_buku = $_POST['judul_buku'];
    $isbn = $_POST['isbn'];
    $pengarang = $_POST['pengarang'];
    $penerbit = $_POST['penerbit'];
    $tahun = $_POST['tahun'];

    // Update data buku berdasarkan ID
    $query = "UPDATE buku SET judul_buku = '$judul_buku', ISBN = '$isbn', pengarang = '$pengarang', penerbit = '$penerbit', tahun = '$tahun' WHERE id_buku = '$id_buku'";
    if ($conn->query($query) === TRUE) {
        echo "Data buku berhasil diupdate.";
    } else {
        echo "Gagal mengupdate data buku: " . $conn->error;
    }
}

// Mendapatkan ID buku dari parameter URL
$id_buku = $_GET['id'];
$bukuData = getBukuByID($id_buku);
?>

<h2>Edit Buku</h2>

<form method="post">
    <input type="hidden" name="id_buku" value="<?php echo $id_buku; ?>">
    <label for="judul_buku">Judul Buku:</label>
    <input type="text" name="judul_buku" id="judul_buku" value="<?php echo $bukuData['judul_buku']; ?>" required>
    <label for="isbn">ISBN:</label>
    <input type="text" name="isbn" id="isbn" value="<?php echo $bukuData['ISBN']; ?>" required>
    <label for="pengarang">Pengarang:</label>
    <input type="text" name="pengarang" id="pengarang" value="<?php echo $bukuData['pengarang']; ?>" required>
    <label for="penerbit">Penerbit:</label>
    <input type="text" name="penerbit" id="penerbit" value="<?php echo $bukuData['penerbit']; ?>" required>
    <label for="tahun">Tahun:</label>
    <input type="text" name="tahun" id="tahun" value="<?php echo $bukuData['tahun']; ?>" required>
    <button type="submit" name="edit_buku">Simpan Perubahan</button>
</form>

<?php include 'includes/footer.php'; ?>
