<!-- data_input.php -->
<?php
include 'includes/header.php';
include 'koneksi.php';

// Function untuk menyimpan data anggota
// Function untuk menyimpan data anggota
// Function untuk menyimpan data anggota
function simpanAnggota($id_anggota, $nama, $jeniskelamin, $alamat, $foto){
    global $conn;
    $query = "INSERT INTO anggota (id_anggota, nama, jeniskelamin, alamat, foto)
        VALUES ('$id_anggota', '$nama', '$jeniskelamin', '$alamat', '$foto')";

if ($conn->query($query) === TRUE) {
    return true;
} else {
    return false;
}
}

// Cek apakah ada data yang dikirim melalui form input anggota
if (isset($_POST['submit_anggota'])) {
    $id_anggota = $_POST['id_anggota'];
    $nama = $_POST['nama'];
    $jeniskelamin = $_POST['jeniskelamin'];
    $alamat = $_POST['alamat'];
    
    $foto = '';
    
    if (isset($_FILES['foto']) && $_FILES['foto']['tmp_name']) {
        $foto = $_FILES['foto']['name'];
        move_uploaded_file($_FILES['foto']['tmp_name'], 'app/images/' . $foto);
    }

    if (simpanAnggota($id_anggota, $nama, $jeniskelamin, $alamat, $foto)) {
        echo "Data anggota berhasil disimpan.";
    } else {
        echo "Data anggota gagal disimpan.";
    }
}

?>
<div class="container-fluid">
    <div class="row">
        <div class="col text-center"> <!-- Tambahkan class text-center di sini -->
            <h2>Input Data Anggota</h2>
            <br><br>
        </div>
    </div>
    
    <!-- Form input anggota -->

    <div class="row input"> <!-- Tambahkan class justify-content-center di sini -->

        <form method="post" action="" enctype="multipart/form-data">
            <div class="form-floating mb-3 col-4 justify-content-center d-flex">
                <input type="text" class="form-control" class="justify-self-center d-flex" id="id_anggota" name="id_anggota" placeholder="Masukkan ID Anggota">
                <label for="id_anggota">ID Anggota</label>
            </div>
            <div class="form-floating mb-3 col-4">
                <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukkan Nama">
                <label for="nama">Nama</label>
            </div>
            <div class="form-floating mb-3 col-4">
                <select class="form-control" id="jeniskelamin" name="jeniskelamin">
                    <option value="L">Laki-Laki</option>
                    <option value="P">Perempuan</option>
                </select>
                <label for="jeniskelamin">Jenis Kelamin</label>
            </div>
            <div class="form-floating col-4 mb-3">
                <input type="text-area" class="form-control" id="alamat" name="alamat" placeholder="Masukkan alamat">
                <label for="alamat">Alamat</label>
            </div>
            
            <!-- Lakukan validasi dan proses upload foto jika diperlukan -->
            <!-- Ganti "path/to/uploaded/foto" dengan lokasi foto yang diupload -->
            <div class="col-4 mb-3">
                <input type="file" class="form-control" id="foto" aria-label="Upload" name="foto">
            </div>
            
            <div class="col-4 tombol">
                <button class="btn btn-outline-success" type="submit" name="submit_anggota" value="Simpan Anggota">Submit</button>
            </div>
        </form>
    </div>
</div>

<br><br><br><br>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet">

<?php include 'includes/footer.php'; ?>
